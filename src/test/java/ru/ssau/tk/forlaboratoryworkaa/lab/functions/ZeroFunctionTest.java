package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class ZeroFunctionTest {
    @Test
    public void test() {
        ZeroFunction firstNumber = new ZeroFunction();
        assertEquals(firstNumber.apply(1.11), 0.00, 1.11);
    }
}