package ru.ssau.tk.forlaboratoryworkaa.lab.operations;

import org.testng.annotations.Test;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.ConstantFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.SqrFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.MathFunction;

import static org.testng.Assert.*;

public class RightSteppingDifferentialOperatorTest {

    @Test
    public void testDerive() {
        ConstantFunction firstNumber = new ConstantFunction(1);
        MathFunction secondNumber=(new RightSteppingDifferentialOperator(0.5)).derive(firstNumber);
        assertEquals(0, secondNumber.apply(0), firstNumber.apply(0));
        secondNumber=(new RightSteppingDifferentialOperator(0.5)).derive(new SqrFunction());
        assertEquals(7.5, secondNumber.apply(3.5), firstNumber.apply(3.5));
    }
}