package ru.ssau.tk.forlaboratoryworkaa.lab.operations;

import org.testng.annotations.Test;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.Point;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.TabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.ArrayTabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.LinkedListTabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.TabulatedFunctionFactory;

import static org.testng.Assert.*;

public class TabulatedFunctionOperationServiceTest  {
    private double[] xValuesOne = new double[]{1, 3, 5};
    private double[] yValuesOne = new double[]{2, 5, 7};
    private double[] yValuesTwo = new double[]{2, 5, 7};

    @Test
    public void testAsPoints() {
        TabulatedFunction array = (new ArrayTabulatedFunctionFactory()).create(xValuesOne, yValuesOne);
        Point[] points = TabulatedFunctionOperationService.asPoints( array);
        int i = 0;
        for (Point point : points) {
            assertEquals(point.x,  array.getX(i));
            assertEquals(point.y,  array.getY(i++));
        }
    }

    @Test
    public void testGetFactory() {
        TabulatedFunctionOperationService tabulatedFunction = new TabulatedFunctionOperationService(new LinkedListTabulatedFunctionFactory());
        assertTrue(tabulatedFunction.getFactory() instanceof LinkedListTabulatedFunctionFactory);
        tabulatedFunction = new TabulatedFunctionOperationService(new ArrayTabulatedFunctionFactory());
        assertTrue(tabulatedFunction.getFactory() instanceof ArrayTabulatedFunctionFactory);
    }

    @Test
    public void testSetFactory() {
        TabulatedFunctionOperationService tabulatedFunctionOperationService = new TabulatedFunctionOperationService(new LinkedListTabulatedFunctionFactory());
        tabulatedFunctionOperationService.setFactory(new LinkedListTabulatedFunctionFactory());
        assertTrue(tabulatedFunctionOperationService.getFactory() instanceof LinkedListTabulatedFunctionFactory);
        tabulatedFunctionOperationService.setFactory(new ArrayTabulatedFunctionFactory());
        assertTrue(tabulatedFunctionOperationService.getFactory() instanceof ArrayTabulatedFunctionFactory);
    }

    @Test
    public void testAdd() {
        TabulatedFunctionFactory arrayFactory = new ArrayTabulatedFunctionFactory();
        TabulatedFunctionFactory listFactory = new LinkedListTabulatedFunctionFactory();
        TabulatedFunctionOperationService tabulatedFunction = new TabulatedFunctionOperationService(new LinkedListTabulatedFunctionFactory());
        TabulatedFunction one = arrayFactory.create(xValuesOne, yValuesOne);
        TabulatedFunction two = arrayFactory.create(xValuesOne, yValuesTwo);
        TabulatedFunction three = tabulatedFunction.add(one, two);
        int i = 0;
        for (Point point : three) {
            assertEquals(point.x, xValuesOne[i]);
            assertEquals(point.y, yValuesOne[i] + yValuesTwo[i++]);
        }
        TabulatedFunction oneList = listFactory.create(xValuesOne, yValuesOne);
        TabulatedFunction twoList = listFactory.create(xValuesOne, yValuesTwo);
        tabulatedFunction = new TabulatedFunctionOperationService(new ArrayTabulatedFunctionFactory());
        TabulatedFunction threeList = tabulatedFunction.add(oneList, twoList);
        int k = 0;
        for (Point point : threeList) {
            assertEquals(point.x, xValuesOne[k]);
            assertEquals(point.y, yValuesOne[k] + yValuesTwo[k++]);
        }
    }

    @Test
    public void testSubtraction() {
        TabulatedFunctionFactory arrayFactory = new ArrayTabulatedFunctionFactory();
        TabulatedFunctionFactory listFactory = new LinkedListTabulatedFunctionFactory();
        TabulatedFunctionOperationService tabulatedFunction = new TabulatedFunctionOperationService(new LinkedListTabulatedFunctionFactory());
        TabulatedFunction one = arrayFactory.create(xValuesOne, yValuesOne);
        TabulatedFunction two = arrayFactory.create(xValuesOne, yValuesTwo);
        TabulatedFunction three = tabulatedFunction.subtraction(one, two);
        int i = 0;
        for (Point point : three) {
            assertEquals(point.x, xValuesOne[i]);
            assertEquals(point.y, yValuesOne[i] - yValuesTwo[i++]);
        }
        TabulatedFunction oneList = listFactory.create(xValuesOne, yValuesOne);
        TabulatedFunction twoList = listFactory.create(xValuesOne, yValuesTwo);
        tabulatedFunction = new TabulatedFunctionOperationService(new ArrayTabulatedFunctionFactory());
        TabulatedFunction threeList = tabulatedFunction.subtraction(oneList, twoList);
        int k = 0;
        for (Point point : threeList) {
            assertEquals(point.x, xValuesOne[k]);
            assertEquals(point.y, yValuesOne[k] - yValuesTwo[k++]);
        }
    }

    @Test
    public void testMultiplication() {
        TabulatedFunctionFactory arrayFactory = new ArrayTabulatedFunctionFactory();
        TabulatedFunctionFactory listFactory = new LinkedListTabulatedFunctionFactory();
        TabulatedFunctionOperationService tabulatedFunction = new TabulatedFunctionOperationService(new LinkedListTabulatedFunctionFactory());
        TabulatedFunction one = arrayFactory.create(xValuesOne, yValuesOne);
        TabulatedFunction two = arrayFactory.create(xValuesOne, yValuesTwo);
        TabulatedFunction three = tabulatedFunction.multiplication(one, two);
        int i = 0;
        for (Point point : three) {
            assertEquals(point.x, xValuesOne[i]);
            assertEquals(point.y, yValuesOne[i] * yValuesTwo[i++]);
        }
        TabulatedFunction oneList = listFactory.create(xValuesOne, yValuesOne);
        TabulatedFunction twoList = listFactory.create(xValuesOne, yValuesTwo);
        tabulatedFunction = new TabulatedFunctionOperationService(new ArrayTabulatedFunctionFactory());
        TabulatedFunction threeList = tabulatedFunction.multiplication(oneList, twoList);
        int k = 0;
        for (Point point : threeList) {
            assertEquals(point.x, xValuesOne[k]);
            assertEquals(point.y, yValuesOne[k] * yValuesTwo[k++]);
        }
    }

    @Test
    public void testDivision() {
        TabulatedFunctionFactory arrayFactory = new ArrayTabulatedFunctionFactory();
        TabulatedFunctionFactory listFactory = new LinkedListTabulatedFunctionFactory();
        TabulatedFunctionOperationService tabulatedFunction = new TabulatedFunctionOperationService(new LinkedListTabulatedFunctionFactory());
        TabulatedFunction one = arrayFactory.create(xValuesOne, yValuesOne);
        TabulatedFunction two = arrayFactory.create(xValuesOne, yValuesTwo);
        TabulatedFunction three = tabulatedFunction.division(one, two);
        int i = 0;
        for (Point point : three) {
            assertEquals(point.x, xValuesOne[i]);
            assertEquals(point.y, yValuesOne[i] / yValuesTwo[i++]);
        }
        TabulatedFunction oneList = listFactory.create(xValuesOne, yValuesOne);
        TabulatedFunction twoList = listFactory.create(xValuesOne, yValuesTwo);
        tabulatedFunction = new TabulatedFunctionOperationService(new ArrayTabulatedFunctionFactory());
        TabulatedFunction threeList = tabulatedFunction.division(oneList, twoList);
        int k = 0;
        for (Point point : threeList) {
            assertEquals(point.x, xValuesOne[k]);
            assertEquals(point.y, yValuesOne[k] / yValuesTwo[k++]);
        }
    }
}
