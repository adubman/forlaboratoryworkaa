package ru.ssau.tk.forlaboratoryworkaa.lab.operations;

import org.testng.annotations.Test;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.Point;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.TabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.ArrayTabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.LinkedListTabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.TabulatedFunctionFactory;

import static org.testng.Assert.*;

public class TabulatedDifferentialOperatorTest {
    private double[] XValues = new double[]{2, 3, 4, 5};
    private double[] YValues = new double[]{2, 5, 8, 11};

    @Test
    public void testDerive() {
        TabulatedFunctionFactory arrayFactory = new ArrayTabulatedFunctionFactory();
        TabulatedFunctionFactory linkedListFactory = new LinkedListTabulatedFunctionFactory();

        TabulatedFunction firstFunction = arrayFactory.create(XValues, YValues);
        TabulatedFunction secondFunction = linkedListFactory.create(XValues, YValues);

        TabulatedDifferentialOperator tabulatedDifferentialOperator = new TabulatedDifferentialOperator();
        TabulatedFunction deriveArray = tabulatedDifferentialOperator.derive(firstFunction);

        for (Point point : deriveArray) {
            assertEquals(point.y, 3.0);
        }
        TabulatedDifferentialOperator tabulatedDifferentialOperatorThroughLinkedList = new TabulatedDifferentialOperator(linkedListFactory);
        TabulatedFunction deriveLinkedList = tabulatedDifferentialOperatorThroughLinkedList.derive(secondFunction);
        for (Point point : deriveLinkedList) {
            assertEquals(point.y, 3.0);
        }
    }
}