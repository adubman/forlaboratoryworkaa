package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class IdentityFunctionTest {
    @Test
    public void test() {
        IdentityFunction firstNumber = new IdentityFunction();
        assertEquals(firstNumber.apply(7.14), 7.14, 0.01);
    }
}

