package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CompositeFunctionTest {
    @Test
    public void testApply() {
        MathFunction firstFunction = new IdentityFunction();
        MathFunction secondFunction = new CoshFunction(); // это гиперболический косинус
        MathFunction function = new CompositeFunction(firstFunction, secondFunction);
        assertEquals(function.apply(2), 3.76, 0.01);


        double[] xValues = new double[]{1, 3, 4};
        double[] yValues = new double[]{2, 1, 3};

        MathFunction tabUsingList = new LinkedListTabulatedFunction(xValues, yValues);
        MathFunction tabUsingArray = new ArrayTabulatedFunction(xValues, yValues);

        MathFunction thirdFunction= new SqrFunction(); // это квадрат
        MathFunction array = new ArrayTabulatedFunction(xValues, yValues);
        assertEquals(array.apply(2), 1.5, 0.1);
        assertEquals(thirdFunction.apply(1.5), 2.25, 0.01);
        assertEquals(array.andThen(thirdFunction).apply(2), 2.25, 0.1);

        MathFunction list = new LinkedListTabulatedFunction(xValues, yValues);
        assertEquals(list.apply(2), 1.5, 0.1);
        assertEquals(secondFunction.apply(1.5), 2.35, 0.01);
        assertEquals(list.andThen(secondFunction).apply(2), 2.35, 0.1);

        assertEquals(firstFunction.andThen(secondFunction).andThen(tabUsingList).andThen(firstFunction).apply(2), 2.5, 0.1);
        assertEquals(firstFunction.andThen(secondFunction).andThen(tabUsingArray).andThen(firstFunction).apply(2), 2.5, 0.1);

    }
}
