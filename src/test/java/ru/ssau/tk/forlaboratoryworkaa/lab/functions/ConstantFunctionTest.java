package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class ConstantFunctionTest {
    @Test
    public void test() {
        ConstantFunction firstNumber = new ConstantFunction(4.11);
        assertEquals(firstNumber.apply(5.11), 4.11, 1.11);
    }
}