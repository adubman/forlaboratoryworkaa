package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.ArrayIsNotSortedException;
import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.DifferentLengthOfArraysException;

import static org.testng.Assert.*;

public class AbstractTabulatedFunctionTest  {
    private final double[] xValuesOne = new double[]{1., 11., 3., 7., 23.};
    private final double[] yValuesOne = new double[]{8., 4, 16., 3., 1., 1.,};
    private double[] xValuesTwo = new double[]{3., 4., 7.};
    private double[] yValuesTwo = new double[]{11., 2., 3.};

    @Test
    public void testCheckLengthIsTheSame() {
        Assert.assertThrows(DifferentLengthOfArraysException.class,
                () -> AbstractTabulatedFunction.checkLengthIsTheSame(xValuesOne, yValuesOne));
    }

    @Test
    public void testCheckSorted() {
        Assert.assertThrows(ArrayIsNotSortedException.class, () -> AbstractTabulatedFunction.checkSorted(xValuesOne));
    }

    @Test
    public void testToString() {
        ArrayTabulatedFunction arrayFun = new ArrayTabulatedFunction(xValuesTwo, yValuesTwo);
        assertEquals(arrayFun.toString(), "ArrayTabulatedFunction size = 3\n[3.0; 11.0]\n[4.0; 2.0]\n[7.0; 3.0]");
        LinkedListTabulatedFunction linkedListFun = new LinkedListTabulatedFunction(xValuesTwo, yValuesTwo);
        assertEquals(linkedListFun.toString(), "LinkedListTabulatedFunction size = 3\n[3.0; 11.0]\n[4.0; 2.0]\n[7.0; 3.0]");
    }
}