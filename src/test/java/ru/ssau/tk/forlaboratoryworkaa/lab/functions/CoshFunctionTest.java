package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class CoshFunctionTest {
    @Test
    public void test() {
        CoshFunction firstNumber = new CoshFunction();
        assertEquals(firstNumber.apply(1.57), 2.51, 0.01);
    }
}