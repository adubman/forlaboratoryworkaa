package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class SqrFunctionTest {
    @Test
    public void test() {
        SqrFunction firstNumber = new SqrFunction();
        assertEquals(firstNumber.apply(1.11), 1.24, 0.24);
    }
}