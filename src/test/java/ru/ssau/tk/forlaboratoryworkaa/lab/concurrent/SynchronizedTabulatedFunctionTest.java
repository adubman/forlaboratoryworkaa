package ru.ssau.tk.forlaboratoryworkaa.lab.concurrent;

import org.testng.annotations.Test;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.ArrayTabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.LinkedListTabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.Point;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.testng.Assert.*;

public class SynchronizedTabulatedFunctionTest {

    private static final double PRECISION = 0.01;
    private double[] xValues = new double[]{1., 4., 7., 10., 13.};
    private double[] yValues = new double[]{4., 6., 8., 10., 12.};

    private SynchronizedTabulatedFunction list = new SynchronizedTabulatedFunction(new LinkedListTabulatedFunction(xValues, yValues));
    private SynchronizedTabulatedFunction array = new SynchronizedTabulatedFunction(new ArrayTabulatedFunction(xValues, yValues));

    private SynchronizedTabulatedFunction initializeUsingTwoArrays() {
        return new SynchronizedTabulatedFunction(new LinkedListTabulatedFunction(xValues, yValues));
    }

    @Test
    public void testTestGetCount() {
        assertEquals(list.getCount(), 5, PRECISION);
        assertEquals(array.getCount(), 5, PRECISION);
    }

    @Test
    public void testTestGetX() {
        assertEquals(list.getX(0), 1, PRECISION);
        assertEquals(array.getX(2), 7, PRECISION);
    }

    @Test
    public void testTestGetY() {
        assertEquals(list.getY(0), 4, PRECISION);
        assertEquals(array.getY(1), 6, PRECISION);
    }

    @Test
    public void testTestSetY() {
        list.setY(3, 16);
        assertEquals(list.getY(3), 16, PRECISION);
        array.setY(4, 16);
        assertEquals(array.getY(4), 16, PRECISION);
    }

    @Test
    public void testTestIndexOfX() {
        assertEquals(list.indexOfX(4), 1, PRECISION);
        assertEquals(list.indexOfX(-1), -1, PRECISION);
        assertEquals(array.indexOfX(7), 2, PRECISION);
        assertEquals(array.indexOfX(-1), -1, PRECISION);
    }

    @Test
    public void testTestIndexOfY() {
        assertEquals(list.indexOfY(6), 1, PRECISION);
        assertEquals(list.indexOfX(-1), -1, PRECISION);
        assertEquals(array.indexOfY(6), 1, PRECISION);
        assertEquals(array.indexOfX(-1), -1, PRECISION);
    }

    @Test
    public void testTestLeftBound() {
        assertEquals(list.leftBound(), 1, PRECISION);
        assertEquals(array.leftBound(), 1, PRECISION);
    }

    @Test
    public void testTestRightBound() {
        assertEquals(list.rightBound(), 13, PRECISION);
        assertEquals(array.rightBound(), 13, PRECISION);
    }

    @Test
    public void testIterator() {
        SynchronizedTabulatedFunction firstNumber = initializeUsingTwoArrays();
        Iterator<Point> iterator = firstNumber.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Point point = iterator.next();
            assertEquals(point.x, firstNumber.getX(i), PRECISION);
            assertEquals(point.y, firstNumber.getY(i++), PRECISION);
        }
        i = 0;
        for (Point point : firstNumber) {
            assertEquals(point.x, firstNumber.getX(i), PRECISION);
            assertEquals(point.y, firstNumber.getY(i++), PRECISION);
        }
        assertThrows(NoSuchElementException.class, iterator::next);
    }

    @Test
    public void testTestApply() {
        assertEquals(list.apply(4), 6, PRECISION);
        assertEquals(array.apply(4), 6, PRECISION);
    }
}