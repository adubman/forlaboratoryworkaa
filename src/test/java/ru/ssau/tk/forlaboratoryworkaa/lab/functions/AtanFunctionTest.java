package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class AtanFunctionTest {
    @Test
    public void test() {
        AtanFunction firstNumber = new AtanFunction();
        assertEquals(firstNumber.apply(0.50), 0.47, 0.34);
    }
}