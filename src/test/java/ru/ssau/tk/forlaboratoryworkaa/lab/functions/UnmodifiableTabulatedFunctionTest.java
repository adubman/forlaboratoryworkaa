package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class UnmodifiableTabulatedFunctionTest {

    private static final double PRECISION = 0.01;
    private double[] xValues = new double[]{1., 4., 7., 10., 13.};
    private double[] yValues = new double[]{4., 6., 8., 10., 12.};
    private TabulatedFunction list = new StrictTabulatedFunction(new LinkedListTabulatedFunction(xValues, yValues));
    private TabulatedFunction array = new StrictTabulatedFunction(new ArrayTabulatedFunction(xValues, yValues));

    @Test
    public void testTestGetCount() {
        assertEquals(list.getCount(), 5, PRECISION);
        assertEquals(array.getCount(), 5, PRECISION);
    }

    @Test
    public void testTestGetX() {
        assertEquals(list.getX(0), 1, PRECISION);
        assertEquals(array.getX(0), 1, PRECISION);
    }

    @Test
    public void testTestGetY() {
        assertEquals(list.getY(0), 4, PRECISION);
        assertEquals(array.getY(0), 4, PRECISION);
    }

    @Test
    public void testTestSetY() {
        list.setY(3, 16);
        assertEquals(list.getY(3), 16, PRECISION);
        array.setY(4, 16);
        assertEquals(array.getY(4), 16, PRECISION);
    }

    @Test
    public void testTestIndexOfX() {
        assertEquals(list.indexOfX(4), 1, PRECISION);
        assertEquals(list.indexOfX(-1), -1, PRECISION);
        assertEquals(array.indexOfX(7), 2, PRECISION);
        assertEquals(array.indexOfX(-1), -1, PRECISION);
    }

    @Test
    public void testTestIndexOfY() {
        assertEquals(list.indexOfY(6), 1, PRECISION);
        assertEquals(list.indexOfX(-1), -1, PRECISION);
        assertEquals(array.indexOfY(6), 1, PRECISION);
        assertEquals(array.indexOfX(-1), -1, PRECISION);
    }

    @Test
    public void testTestLeftBound() {
        assertEquals(list.leftBound(), 1, PRECISION);
        assertEquals(array.leftBound(), 1, PRECISION);
    }

    @Test
    public void testTestRightBound() {
        assertEquals(list.rightBound(), 13, PRECISION);
        assertEquals(array.rightBound(), 13, PRECISION);
    }
    @Test
    public void testTestIterator() {
        assertEquals(list.iterator().next().x, 1, PRECISION);
        assertEquals(array.iterator().next().x, 1, PRECISION);
    }

    @Test
    public void testTestApply() {
        assertEquals(list.apply(4), 6, PRECISION);
        assertThrows(UnsupportedOperationException.class, () -> list.apply(2.5));

        assertEquals(array.apply(4), 6, PRECISION);
        assertThrows(UnsupportedOperationException.class, () -> array.apply(2.5));
    }
}