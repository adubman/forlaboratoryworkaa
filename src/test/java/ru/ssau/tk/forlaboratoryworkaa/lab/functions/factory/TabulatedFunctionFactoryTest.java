package ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory;

import org.testng.annotations.Test;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.*;

import static org.testng.Assert.*;

public class TabulatedFunctionFactoryTest {
    private static final double PRECISION = 0.01;
    private double[] xValues = new double[]{1., 4., 7., 10., 13.};
    private double[] yValues = new double[]{4., 6., 8., 10., 12.};

    @Test
    public void testCreate() {
        TabulatedFunctionFactory firstNumber = new ArrayTabulatedFunctionFactory();
        TabulatedFunctionFactory secondNumber = new LinkedListTabulatedFunctionFactory();

        TabulatedFunction function = firstNumber.create(xValues, yValues);
        assertTrue(function instanceof ArrayTabulatedFunction);   /*Оператор instanceof нужен для того,
        чтобы проверить, был ли объект, на которую ссылается переменная X,
         создан на основе какого-либо класса Y.
         Оператор instanceof проверяет именно происхождение объекта, а не переменной.*/
        function = secondNumber.create(xValues, yValues);
        assertTrue(function instanceof LinkedListTabulatedFunction);
    }

    @Test
    public void testCreateStrict() {
        TabulatedFunctionFactory firstNumber = new ArrayTabulatedFunctionFactory();
        TabulatedFunction array = (firstNumber).createStrict(xValues, yValues);

        TabulatedFunctionFactory secondNumber = new LinkedListTabulatedFunctionFactory();
        TabulatedFunction list = (secondNumber).createStrict(xValues, yValues);

        assertTrue(array instanceof StrictTabulatedFunction);
        assertEquals(array.getCount(), 5);
        assertEquals(array.getX(0), 1, PRECISION);
        assertEquals(array.getY(1), 6, PRECISION);
        assertEquals(array.indexOfX(7), 2, PRECISION);
        assertEquals(array.indexOfY(10), 3, PRECISION);
        assertEquals(array.leftBound(), 1, PRECISION);
        assertEquals(array.rightBound(), 13, PRECISION);
        array.setY(4, 13);
        assertEquals(array.getY(4), 13, PRECISION);
        assertEquals(array.iterator().next().x, 1, PRECISION);
        assertEquals(array.apply(4), 6, PRECISION);
        assertThrows(UnsupportedOperationException.class, () -> array.apply(2.5));

        assertTrue(list instanceof StrictTabulatedFunction);
        assertEquals(list.getX(0), 1, PRECISION);
        assertEquals(list.getY(1), 6, PRECISION);
        assertEquals(list.indexOfX(7), 2, PRECISION);
        assertEquals(list.indexOfY(10), 3, PRECISION);
        assertEquals(list.leftBound(), 1, PRECISION);
        assertEquals(list.rightBound(), 13, PRECISION);
        list.setY(4, 13);
        assertEquals(list.getY(4), 13, PRECISION);
        assertEquals(list.iterator().next().x, 1, PRECISION);
        assertEquals(list.apply(4), 6, PRECISION);
        assertThrows(UnsupportedOperationException.class, () -> list.apply(2.5));
    }


    @Test
    public void testCreateStrictUnmodifiable() {
        TabulatedFunctionFactory arrayTabulatedFunctionFactory = new ArrayTabulatedFunctionFactory();
        TabulatedFunction function = arrayTabulatedFunctionFactory.createStrictUnmodifiable(xValues, yValues);

        assertTrue(function instanceof UnmodifiableTabulatedFunction);
        assertEquals(function.getCount(), 5);
        assertEquals(function.getX(0), 1, PRECISION);
        assertEquals(function.getY(1), 6, PRECISION);
        assertEquals(function.indexOfX(7), 2, PRECISION);
        assertEquals(function.indexOfY(10), 3, PRECISION);
        assertEquals(function.leftBound(), 1, PRECISION);
        assertEquals(function.rightBound(), 13, PRECISION);
        assertEquals(function.apply(4), 6, PRECISION);
        assertEquals(function.iterator().next().x, 1, PRECISION);
        assertThrows(UnsupportedOperationException.class, () -> function.setY(4, 13));
        assertThrows(UnsupportedOperationException.class, () -> function.apply(2.5));
    }
}