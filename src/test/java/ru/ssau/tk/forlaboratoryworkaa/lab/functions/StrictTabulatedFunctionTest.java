package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.testng.Assert.*;

public class StrictTabulatedFunctionTest {
    private double[] xValues = new double[]{1., 4., 7., 10., 13.};
    private double[] yValues = new double[]{4., 6., 8., 10., 12.};
    private TabulatedFunction array = new StrictTabulatedFunction(new ArrayTabulatedFunction(xValues, yValues));
    private double PRECISION = 0.001;

    @Test
    public void testTestGetCount() {
        assertEquals(array.getCount(), 5, PRECISION);
    }

    @Test
    public void testTestGetX() {
        assertEquals(array.getX(0), 1, PRECISION);
    }

    @Test
    public void testTestGetY() {
        assertEquals(array.getY(0), 4, PRECISION);
    }

    @Test
    public void testTestSetY() {
        array.setY(3, 16);
        assertEquals(array.getY(3), 16, PRECISION);
    }

    @Test
    public void testTestIndexOfX() {
        assertEquals(array.indexOfX(4), 1, PRECISION);
        assertEquals(array.indexOfX(-1), -1, PRECISION);
    }

    @Test
    public void testTestIndexOfY() {
        assertEquals(array.indexOfY(6), 1, PRECISION);
        assertEquals(array.indexOfX(-1), -1, PRECISION);
    }

    @Test
    public void testTestLeftBound() {
        assertEquals(array.leftBound(), 1, PRECISION);
    }

    @Test
    public void testTestRightBound() {
        assertEquals(array.rightBound(), 13, PRECISION);
    }

    @Test
    public void testTestIterator() {
        Iterator<Point> iterator = array.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Point point = iterator.next();
            assertEquals(point.x, array.getX(i), PRECISION);
            assertEquals(point.y, array.getY(i++), PRECISION);
        }
        i = 0;
        for (Point point : array) {
            assertEquals(point.x, array.getX(i), PRECISION);
            assertEquals(point.y, array.getY(i++), PRECISION);
        }
        assertThrows(NoSuchElementException.class, iterator::next);
    }

    @Test
    public void testTestApply() {
        assertEquals(array.apply(4), 6, PRECISION);
        assertThrows(UnsupportedOperationException.class, () -> array.apply(2.5));
    }
}