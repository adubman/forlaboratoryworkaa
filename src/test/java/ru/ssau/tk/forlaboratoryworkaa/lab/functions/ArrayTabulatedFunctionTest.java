package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.ArrayIsNotSortedException;
import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.DifferentLengthOfArraysException;
import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.InterpolationException;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.testng.Assert.*;


public class ArrayTabulatedFunctionTest {
    private double[] xValues = new double[]{1., 4., 7., 10., 13.};
    private double[] yValues = new double[]{4., 6., 8., 10., 12.};
    private double[] zValues = new double[]{4., 6., 8., 10.};
    private double[] wValues = new double[]{11., 6., 8., 10., 12.};

    private double PRECISION = 0.1;
    private MathFunction Function = new SqrFunction();

    private ArrayTabulatedFunction initializeUsingTwoArrays() {
        return new ArrayTabulatedFunction(xValues, yValues);
    }

    private ArrayTabulatedFunction initializeUsingMathFunction() {
        return new ArrayTabulatedFunction(Function, 10, 0, 11);
    }

    @Test
    public void testDifferentLengthOfArraysExceptionInitializeUsingTwoArrays() {
        assertThrows(DifferentLengthOfArraysException.class, () -> {
            new ArrayTabulatedFunction(zValues, yValues);
        });
    }

    @Test
    public void testArrayIsNotSortedExceptionInitializeUsingTwoArrays() {
        assertThrows(ArrayIsNotSortedException.class, () -> {
            new ArrayTabulatedFunction(wValues, yValues);
        });
    }


    @Test
    public void testIllegalArgumentExceptionInArrayInitializeThroughTwoArrays() {
        assertThrows(IllegalArgumentException.class, () -> {
            new ArrayTabulatedFunction(new double[]{1}, new double[]{2});
        });
    }

    @Test
    public void testIllegalArgumentExceptionInArrayInitializeUsingMathFunction() {
        assertThrows(IllegalArgumentException.class, () -> {
            new ArrayTabulatedFunction(Function, 0, 0, 1);
        });
    }

    @Test
    public void testGetCount() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.getCount(), 5, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.getCount(), 11, PRECISION);
    }

    @Test
    public void testLeftBound() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.leftBound(), 1, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.leftBound(), 0, PRECISION);
    }

    @Test
    public void testRightBound() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.rightBound(), 13, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.rightBound(), 10, PRECISION);
    }

    @Test
    public void testGetX() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.getX(1), 4, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.getX(2), 2, PRECISION);
        for (int i = 0; i < 5; i++) {
            assertEquals(firstNumber.getX(i), 1 + 3 * i, PRECISION);
        }

        assertEquals(firstNumber.getX(0), 1, PRECISION);
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            firstNumber.getX(-1);
        });
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            secondNumber.getX(32);
        });
    }

    @Test
    public void testGetY() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.getY(0), 4, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.getY(3), 9, PRECISION);
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            firstNumber.getY(-2);
        });
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            secondNumber.getY(43);
        });
    }

    @Test
    public void testSetY() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        firstNumber.setY(3, 16);
        assertEquals(firstNumber.getY(3), 16, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        secondNumber.setY(7, 3);
        assertEquals(secondNumber.getY(7), 3, PRECISION);
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            firstNumber.setY(-2, 10);
        });
        Assert.assertThrows(IllegalArgumentException.class, () -> {
            secondNumber.setY(-4, 11);
        });
    }

    @Test
    public void testIndexOfX() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.indexOfX(4), 1, PRECISION);
        assertEquals(firstNumber.indexOfX(-1), -1, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.indexOfX(1), 1, PRECISION);
        assertEquals(secondNumber.indexOfX(-1), -1, PRECISION);
    }

    @Test
    public void testIndexOfY() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.indexOfY(6), 1, PRECISION);
        assertEquals(firstNumber.indexOfX(-1), -1, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.indexOfY(4), 2, PRECISION);
        assertEquals(secondNumber.indexOfX(-1), -1, PRECISION);
    }

    @Test
    public void testFloorIndexOfX() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.floorIndexOfX(4.1), 1, PRECISION);
        assertEquals(firstNumber.floorIndexOfX(1.2), 0, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.floorIndexOfX(0.3), 0, PRECISION);
        assertEquals(secondNumber.floorIndexOfX(1.2), 1, PRECISION);
        Assert.assertThrows(IllegalArgumentException.class, () -> firstNumber.floorIndexOfX(-2));
    }

    @Test
    public void testExtrapolateLeft() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.extrapolateLeft(1.1), 4.1, PRECISION);
        assertEquals(firstNumber.extrapolateLeft(1.3), 4.2, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.extrapolateLeft(2), 2, PRECISION);
        assertEquals(secondNumber.extrapolateLeft(2.2), 2.2, PRECISION);
    }

    @Test
    public void testExtrapolateRight() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.extrapolateRight(6), 7.3, PRECISION);
        assertEquals(firstNumber.extrapolateRight(6.5), 7.7, PRECISION);
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.extrapolateRight(12), 138, PRECISION);
        assertEquals(secondNumber.extrapolateRight(12.2), 141.8, PRECISION);
    }

    @Test
    public void testInterpolate() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        assertEquals(firstNumber.interpolate(2.5, 0), 5, PRECISION);
        assertEquals(firstNumber.interpolate(2.6, 0), 5, PRECISION);
        Assert.assertThrows(InterpolationException.class, () -> {
            firstNumber.interpolate(9, 1);
        });
        ArrayTabulatedFunction secondNumber = initializeUsingMathFunction();
        assertEquals(secondNumber.interpolate(1.5, 1), 2.5, PRECISION);
        assertEquals(secondNumber.interpolate(1.6, 1), 2.8, PRECISION);
        Assert.assertThrows(InterpolationException.class, () -> {
            secondNumber.interpolate(7, 1);
        });
    }


    @Test
    public void testIterator() {
        ArrayTabulatedFunction firstNumber = initializeUsingTwoArrays();
        Iterator<Point> iterator = firstNumber.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            Point point = iterator.next();
            assertEquals(point.x, firstNumber.getX(i), PRECISION);
            assertEquals(point.y, firstNumber.getY(i++), PRECISION);
        }
        i = 0;
        for (Point point : firstNumber) {
            assertEquals(point.x, firstNumber.getX(i), PRECISION);
            assertEquals(point.y, firstNumber.getY(i++), PRECISION);
        }
        assertThrows(NoSuchElementException.class, iterator::next);
    }
}