package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class UnitFunctionTest {
    @Test
    public void test() {
        UnitFunction firstNumber = new UnitFunction();
        assertEquals(firstNumber.apply(1.11), 1.00, 1.11);
    }
}