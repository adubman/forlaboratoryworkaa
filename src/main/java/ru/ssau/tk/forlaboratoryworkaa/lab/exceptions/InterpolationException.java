package ru.ssau.tk.forlaboratoryworkaa.lab.exceptions;

public class InterpolationException extends RuntimeException {
    private static final long serialVersionUID = -9076767415402588627L;

    public InterpolationException() {
    }

    public InterpolationException(String message) {
        super(message);
    }

}
