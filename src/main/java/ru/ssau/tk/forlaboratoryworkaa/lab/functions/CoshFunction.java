package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import static java.lang.Math.*;

public class CoshFunction implements MathFunction {
    public double apply(double x) {
        return cosh(x);
    }
}