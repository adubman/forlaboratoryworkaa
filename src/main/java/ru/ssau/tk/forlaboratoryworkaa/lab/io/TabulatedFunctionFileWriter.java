package ru.ssau.tk.forlaboratoryworkaa.lab.io;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.ArrayTabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.LinkedListTabulatedFunction;
import java.io.*;

public class TabulatedFunctionFileWriter {
    public static void main(String[] args) {
        double[] xValues={3.4,4.5,5.5};
        double[] yValues={1.3,3.3,1.1};
        ArrayTabulatedFunction firstFunction=new ArrayTabulatedFunction(xValues,yValues);
        LinkedListTabulatedFunction secondFunction=new LinkedListTabulatedFunction(xValues,yValues);
        try (BufferedWriter fileWriterArrayTabulatedFunction = new BufferedWriter(new FileWriter(new File("output/array function.txt")))) {
            FunctionsIO.writeTabulatedFunction(fileWriterArrayTabulatedFunction, firstFunction);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedWriter fileWriterLinkedListTabulatedFunction = new BufferedWriter(new FileWriter(new File("output/linked list function.txt")))) {
            FunctionsIO.writeTabulatedFunction(fileWriterLinkedListTabulatedFunction, secondFunction);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
