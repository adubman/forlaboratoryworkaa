package ru.ssau.tk.forlaboratoryworkaa.lab.operations;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.Point;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.TabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.ArrayTabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.TabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.InconsistentFunctionsException;


public class TabulatedFunctionOperationService {

    private TabulatedFunctionFactory factory;

    public TabulatedFunctionOperationService(TabulatedFunctionFactory factory) {

        this.factory = factory;
    }

    public TabulatedFunctionFactory getFactory() {
        return factory;
    }

    public void setFactory(TabulatedFunctionFactory factory) {
        this.factory = factory;
    }

    public TabulatedFunctionOperationService() {
        factory = new ArrayTabulatedFunctionFactory();
    }

    public static Point[] asPoints(TabulatedFunction tabulatedFunction) {
        Point[] points = new Point[tabulatedFunction.getCount()];
        int i = 0;
        for (Point presentPoint : tabulatedFunction) {
            points[i++] = presentPoint;
        }
        return points;
    }

    private interface BiOperation {
        double apply(double u, double v);
    }

    private TabulatedFunction doOperation(TabulatedFunction a, TabulatedFunction b, BiOperation operation) {
        int n = a.getCount();
        if (n != b.getCount()) {
            throw new InconsistentFunctionsException();
        }
        Point[] aPoints = asPoints(a);
        Point[] bPoints = asPoints(b);
        double[] xValues = new double[n];
        double[] yValues = new double[n];
        for (int i = 0; i < n; i++) {
            if (aPoints[i].x != bPoints[i].x) {
                throw new InconsistentFunctionsException();
            }
            xValues[i] = aPoints[i].x;
            yValues[i] = operation.apply(aPoints[i].y, bPoints[i].y);
        }
        return factory.create(xValues, yValues);
    }

    TabulatedFunction add(TabulatedFunction a, TabulatedFunction b) {
        BiOperation outcome = new BiOperation() {
            @Override
            public double apply(double u, double v) { return u + v;}
        };
        return doOperation(a, b, outcome);
    }

    TabulatedFunction subtraction(TabulatedFunction a, TabulatedFunction b) {
        BiOperation outcome = new BiOperation() {
            @Override
            public double apply(double u, double v) { return u - v;}
        };
        return doOperation(a, b, outcome);
    }

    TabulatedFunction multiplication(TabulatedFunction a, TabulatedFunction b) {
        BiOperation outcome = new BiOperation() {
            @Override
            public double apply(double u, double v) { return u * v;}
        };
        return doOperation(a, b, outcome);
    }

    TabulatedFunction division(TabulatedFunction a, TabulatedFunction b) {
        BiOperation outcome = new BiOperation() {
            @Override
            public double apply(double u, double v) { return u / v;}
        };
        return doOperation(a, b, outcome);
    }

}