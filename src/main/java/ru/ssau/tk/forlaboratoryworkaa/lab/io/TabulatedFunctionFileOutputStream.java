package ru.ssau.tk.forlaboratoryworkaa.lab.io;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.ArrayTabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.LinkedListTabulatedFunction;

import java.io.*;

public class TabulatedFunctionFileOutputStream {

    public static void main(String[] args) {
        double[] xValues = {1, 6, 11};
        double[] yValues = {1, 6, 11};
        ArrayTabulatedFunction arrayTabulatedFunction = new ArrayTabulatedFunction(xValues, yValues);
        LinkedListTabulatedFunction linkedListTabulatedFunction = new LinkedListTabulatedFunction(xValues, yValues);
        try (BufferedOutputStream outputStreamArrayTabulatedFunction = new BufferedOutputStream(new FileOutputStream(new File("output/array function.bin")))) {
            FunctionsIO.writeTabulatedFunction(outputStreamArrayTabulatedFunction, arrayTabulatedFunction);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedOutputStream outputStreamLinkedListTabulatedFunction = new BufferedOutputStream(new FileOutputStream(new File("output/linked list function.bin")))) {
            FunctionsIO.writeTabulatedFunction(outputStreamLinkedListTabulatedFunction, linkedListTabulatedFunction);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

