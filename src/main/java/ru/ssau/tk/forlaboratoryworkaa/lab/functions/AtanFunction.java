package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import static java.lang.Math.*;

public class AtanFunction implements MathFunction {
    public double apply(double x) {
    return atan(x);
}
}