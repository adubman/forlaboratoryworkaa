package ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.LinkedListTabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.MathFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.TabulatedFunction;

public class LinkedListTabulatedFunctionFactory implements TabulatedFunctionFactory {

    @Override
    public TabulatedFunction create(double[] xValues, double[] yValues) {
        return new LinkedListTabulatedFunction(xValues, yValues);
    }

    @Override
    public TabulatedFunction create(MathFunction source, double xFrom, double xTo, int count) {
        return new LinkedListTabulatedFunction(source, xFrom, xTo, count);
    }
    @Override
    public TabulatedFunction getIdentity() {
        return LinkedListTabulatedFunction.getIdentity();
    }

}