package ru.ssau.tk.forlaboratoryworkaa.lab.ui;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.ArrayTabulatedFunctionFactory;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class MyFrame extends JFrame {
    private JButton buttonOK = new JButton("OK");
    private JTextField input = new JTextField("");
    private JLabel labelInput = new JLabel("Input:");
    private JLabel labelTable = new JLabel("Table");
    private JButton countUp = new JButton("count up");
    private TableModelUsingArray tableModel = new TableModelUsingArray();
    private JTable table = new JTable(tableModel);

    private void compose() {
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        JScrollPane tableScrollPane = new JScrollPane(table);

        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonOK)
                        .addComponent(labelInput)
                        .addComponent(input)
                        .addComponent(labelTable)
                        .addComponent(countUp))
                .addComponent(tableScrollPane)
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(buttonOK)
                        .addComponent(labelInput)
                        .addComponent(input)
                        .addComponent( labelTable)
                        .addComponent(countUp))
                .addComponent(tableScrollPane)
        );

    }

    MyFrame() {
        super("Window");
        this.setBounds(300, 300, 500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        compose();
        buttonOK.addActionListener(new ButtonEventListener());
        countUp.addActionListener(new ButtonTwoEventListener());
        buttonOK.setEnabled(true);
        countUp.setEnabled(true);
    }

    class ButtonEventListener extends Component implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                String text = input.getText();
                int count = Integer.parseInt(text);
                tableModel.initArrays(count);
                System.out.println(tableModel.toString());
                tableModel.fireTableDataChanged();
                setVisible(true);
            } catch (Exception exc) {
                ErrorWindows errorWindow = new ErrorWindows(this, exc);
                errorWindow.showErrorWindow(this, exc);
            }
        }
    }

    class ButtonTwoEventListener extends Component implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String text = input.getText();
                int count = Integer.parseInt(text);
                ArrayTabulatedFunctionFactory factory;
                factory = new ArrayTabulatedFunctionFactory();
                factory.create(tableModel.getxValues(), tableModel.getxValues());
                System.out.println(factory.toString());
                setVisible(false);
            } catch (Exception ex) {
                ErrorWindows errorWindow = new ErrorWindows(this, ex);
                errorWindow.showErrorWindow(this, ex);
            }
        }
    }

    public static void main(String[] args) {
        MyFrame app = new MyFrame();
        new TableModelUsingArray();
        app.setVisible(true);
    }
}


