package ru.ssau.tk.forlaboratoryworkaa.lab.operations;


import ru.ssau.tk.forlaboratoryworkaa.lab.functions.MathFunction;

public class RightSteppingDifferentialOperator extends SteppingDifferentialOperator {

    protected double step;

    public RightSteppingDifferentialOperator(double step) {
        super(step);
    };

    @Override
    public MathFunction derive(MathFunction function){
        MathFunction derivative = new MathFunction() {
            @Override
            public double apply(double x) {
                return (function.apply(x+step)-function.apply(x))/step;
            };
        };
        return derivative;
    };
}
