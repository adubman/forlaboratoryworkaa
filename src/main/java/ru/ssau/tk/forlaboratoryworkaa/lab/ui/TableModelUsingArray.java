package ru.ssau.tk.forlaboratoryworkaa.lab.ui;

import javax.swing.table.AbstractTableModel;

public class TableModelUsingArray extends AbstractTableModel {
    private static final int X_NUMBER = 0;
    private static final int Y_NUMBER = 1;
    private double[] xValues;
    private double[] yValues;

    void initArrays(int count) {
        xValues = new double[count];
        yValues = new double[count];
    }

    @Override
    public void setValueAt(Object bValue, int rowIndex, int columnIndex) throws NumberFormatException {
        if (columnIndex == X_NUMBER) {
            try {
                xValues[rowIndex] = (Double) bValue;
            } catch (Exception e) {
                assert xValues != null;
                xValues[rowIndex] = 0.0;
            }
        } else if (columnIndex == Y_NUMBER) {
            try {
                yValues[rowIndex] = (Double) bValue;
            } catch (Exception e) {
                assert yValues != null;
                yValues[rowIndex] = 0.0;
            }
        }
    }

    @Override
    public int getRowCount() {
        if (xValues != null) {
            return xValues.length;
        } else {
            return 0;
        }
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case X_NUMBER:
                return "X";
            case Y_NUMBER:
                return "Y";
        }
        return super.getColumnName(column);
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return Double.class;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return xValues[rowIndex];
            case 1:
                return yValues[rowIndex];
        }
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case X_NUMBER:
            case Y_NUMBER:
                return true;
        }
        return false;
    }

    public double[] getxValues() {
        return xValues;
    }

    public double[] getyValues() {
        return yValues;
    }

}