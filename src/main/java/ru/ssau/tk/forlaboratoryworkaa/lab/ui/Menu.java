package ru.ssau.tk.forlaboratoryworkaa.lab.ui;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import static ru.ssau.tk.forlaboratoryworkaa.lab.io.FunctionsIO.deserialize;
import static ru.ssau.tk.forlaboratoryworkaa.lab.io.FunctionsIO.serialize;

public class Menu extends JFrame {
    private JButton createTableButton = new JButton("Create  Table");
    private JButton createFromMathFunctionButton = new JButton("Create from Math function");
    JFrame jFrame = new JFrame("File");
    private JButton saveButton = new JButton("Save");
    private JButton openButton = new JButton("Open");
    private TableModelUsingTabulatedFunction tableModel = new TableModelUsingTabulatedFunction();
    private JTable table = new JTable(tableModel);

    private Menu(String table) {
        setTitle("Menu");
        setBounds(300, 200, 500, 200);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        actionPerformed();
        actionPerformedTwo();
        actionPerformedThree();
        actionPerformedFour();
        compose();
    }

    public static void main(String[] args) {
        Menu window = new Menu("Table");
        window.setVisible(true);
    }

    private void actionPerformed() {
        createTableButton.addActionListener(event -> {
                    try {
                        MyFrame windowOne = new MyFrame();
                        windowOne.setVisible(true);
                    } catch (Exception e) {
                        new ErrorWindows(this, e);
                    }
                }
        );
    }

    private void actionPerformedTwo() {
        createFromMathFunctionButton.addActionListener(event -> {
                    try {
                        MathWindow windowTwo = new MathWindow(tableModel);
                        windowTwo.setVisible(true);
                    } catch (Exception e) {
                        new ErrorWindows(this, e);
                    }
                }
        );
    }

    private void actionPerformedThree() {
        JFileChooser fileChooserSave = new JFileChooser();
        fileChooserSave.setDialogTitle("Save file");
        fileChooserSave.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooserSave.addChoosableFileFilter(new FileNameExtensionFilter("Binary files", "bin"));
        fileChooserSave.setAcceptAllFileFilterUsed(true);
        saveButton.addActionListener(e -> {
            fileChooserSave.showSaveDialog(jFrame);
            File file = fileChooserSave.getSelectedFile();
            if (file != null) {
                try (BufferedOutputStream outPutStream = new BufferedOutputStream(new FileOutputStream(file))) {
                    serialize(outPutStream, tableModel.getTabulatedFunction());
                } catch (IOException ex) {
                    ErrorWindows errorWindow = new ErrorWindows(this, ex);
                    errorWindow.showErrorWindow(this, ex);
                }
            }
        });
    }

    private void actionPerformedFour() {
        JFileChooser fileChooserOpen = new JFileChooser();
        fileChooserOpen.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooserOpen.setDialogTitle("Open file");
        fileChooserOpen.addChoosableFileFilter(new FileNameExtensionFilter("Binary files", "bin"));
        fileChooserOpen.setAcceptAllFileFilterUsed(true);
        openButton.addActionListener(e -> {
            fileChooserOpen.showOpenDialog(jFrame);
            File file = fileChooserOpen.getSelectedFile();
            if (file != null) {
                try (BufferedInputStream inPutStream = new BufferedInputStream(new FileInputStream(file))) {
                    tableModel.setTabulatedFunction(deserialize(inPutStream));
                    tableModel.fireTableDataChanged();
                } catch (IOException | ClassNotFoundException ex) {
                    ErrorWindows errorWindow = new ErrorWindows(this, ex);
                    errorWindow.showErrorWindow(this, ex);
                }
            }
        });
    }

    private void compose() {
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        JScrollPane tableScrollPane = new JScrollPane(table);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(createTableButton)
                        .addComponent(createFromMathFunctionButton)
                        .addComponent(saveButton)
                        .addComponent(openButton)
                        .addComponent(tableScrollPane))
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(createTableButton)
                        .addComponent(createFromMathFunctionButton)
                        .addComponent(saveButton)
                        .addComponent(openButton)
                        .addComponent(tableScrollPane))
        );
    }
}
