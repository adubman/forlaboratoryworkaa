package ru.ssau.tk.forlaboratoryworkaa.lab.io;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.ArrayTabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.LinkedListTabulatedFunctionFactory;
import java.io.*;

public class TabulatedFunctionFileReader {
    public static void main(String[] args) {
        try (BufferedReader fileReaderArrayTabulatedFunction = new BufferedReader(new FileReader("input/function.txt"))) {
            System.out.println((FunctionsIO.readTabulatedFunction(fileReaderArrayTabulatedFunction, new ArrayTabulatedFunctionFactory())).toString());
        } catch (IOException e) {
            e.printStackTrace();
        };
        try (BufferedReader fileReaderLinkedListTabulatedFunction = new BufferedReader(new FileReader("input/function.txt"))) {
            System.out.println((FunctionsIO.readTabulatedFunction(fileReaderLinkedListTabulatedFunction, new LinkedListTabulatedFunctionFactory())).toString());
        } catch (IOException e) {
            e.printStackTrace();
        };
    };
}