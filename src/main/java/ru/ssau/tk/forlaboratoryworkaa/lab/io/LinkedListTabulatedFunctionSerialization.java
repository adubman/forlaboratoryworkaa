package ru.ssau.tk.forlaboratoryworkaa.lab.io;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.LinkedListTabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.TabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.LinkedListTabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.operations.TabulatedDifferentialOperator;

import java.io.*;

import static ru.ssau.tk.forlaboratoryworkaa.lab.io.FunctionsIO.deserialize;
import static ru.ssau.tk.forlaboratoryworkaa.lab.io.FunctionsIO.serialize;

public class LinkedListTabulatedFunctionSerialization {
    public static void main(String[] args) {
        double[] xValues = {1, 6, 11};
        double[] yValues = {1, 6, 11};
        LinkedListTabulatedFunction function = new LinkedListTabulatedFunction(xValues, yValues);
        TabulatedDifferentialOperator differentialOperator = new TabulatedDifferentialOperator(new LinkedListTabulatedFunctionFactory());
        TabulatedFunction firstDerive = differentialOperator.derive(function);
        TabulatedFunction secondDerive = differentialOperator.derive(firstDerive);
        try (BufferedOutputStream outPutStream = new BufferedOutputStream(new FileOutputStream("output/serialized linked list functions.bin"))) {
            serialize(outPutStream, function);
            serialize(outPutStream, firstDerive);
            serialize(outPutStream, secondDerive);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (BufferedInputStream inPutStream = new BufferedInputStream(new FileInputStream("output/serialized linked list functions.bin"))) {
            System.out.println(deserialize(inPutStream).toString() + '\n' + deserialize(inPutStream).toString() + '\n' + deserialize(inPutStream).toString());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
