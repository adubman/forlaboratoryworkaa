package ru.ssau.tk.forlaboratoryworkaa.lab.ui;

import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.ArrayIsNotSortedException;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.NotSerializableException;

class ErrorWindows {

    ErrorWindows(Component parent, Exception e) {
        showErrorWindow(parent, e);
    }

    void showErrorWindow(Component parent, Exception e) {
        String head = generateMessageForException(e);
        JOptionPane.showMessageDialog(parent, head, "Error", JOptionPane.ERROR_MESSAGE);
    }

    private String generateMessageForException(Exception e) {
        e.printStackTrace();
        if (e instanceof NumberFormatException) {
            return "Number Format Exception: " + e.getMessage();
        }
        if (e instanceof ArrayIsNotSortedException) {
            return "Array Is Not Sorted Exception: " + e.getMessage();
        }
        if (e instanceof FileNotFoundException) {
            return "File Not Found Exception: " + e.getMessage();
        }
        if (e instanceof NotSerializableException) {
            return "Not Serializable Exception: " + e.getMessage();
        }
        if (e instanceof IOException){
            return "IOException: " + e.getMessage();
        }
        return "Unknown error: " + e.getMessage();
    }

}