package ru.ssau.tk.forlaboratoryworkaa.lab.functions;


import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.InterpolationException;

import java.io.Serializable;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedListTabulatedFunction extends AbstractTabulatedFunction implements Serializable, Removable {

    private static final long serialVersionUID = 9088180304183031515L;
    private int count;
    private Node head;

    public LinkedListTabulatedFunction() {

    }

    public static TabulatedFunction getIdentity() {
        return new LinkedListTabulatedFunction();
    }

    @Override
    public void remove(int index) throws IllegalArgumentException {
        Node node;
        node = getNode(index);
        if ( node == null) {
            throw new IllegalArgumentException("index went beyond");
        }
        Node early =  node.prev;
        Node following = node.next;
        early.next = following;
        following.prev = early;
        count--;
    }

    public static class Node implements Serializable {
        private static final long serialVersionUID = 8201778232891339206L;
        Node next;
        Node prev;
        double x;
        double y;
    }

    void addNode(double x, double y) {
        var node = new Node();
        node.x = x;
        node.y = y;
        if (head == null) {
            head = node;
            node.next = node;
            node.prev = node;
        } else {
            node.prev = head.prev;
            node.next = head;
            head.prev.next = node;
        }
        head.prev = node;
        count++;

    }

    public int getCount() {
        return count;
    }

    public double leftBound() {
        return head.x;
    }

    public double rightBound() {
        return head.prev.x;
    }



    // public LinkedListTabulatedFunction(double[] xValues, double[] yValues) throws IllegalArgumentException {
   public LinkedListTabulatedFunction(double[] xValues, double[] yValues) {
        checkLengthIsTheSame(xValues,yValues);
        checkSorted(xValues);
        if (xValues.length < 2) {
            throw new IllegalArgumentException("the number of points is less than two");
        }

        for (int i = 0; i < xValues.length; i++) {
            this.addNode(xValues[i], yValues[i]);
        }
    }

   // public LinkedListTabulatedFunction(MathFunction source, double xFrom, double xTo, int count) throws IllegalArgumentException {
   public LinkedListTabulatedFunction(MathFunction source, double xFrom, double xTo, int count){
    if (count < 2) {
            throw new IllegalArgumentException("the number of points is less than two");
        }
        if (xFrom > xTo) {
            double xOdds;
            xOdds = xFrom - xTo;
            xTo = xTo + xOdds;
            xFrom = xFrom - xOdds;
        }

        double step = (xTo - xFrom) / (count - 1);
        double adding = xFrom;
        if (xFrom != xTo) {
            for (int i = 0; i < count; i++) {
                this.addNode(adding, source.apply(adding));
                adding = adding + step;
            }
        } else {
            for (int i = 0; i < count; i++) {
                this.addNode(xFrom, source.apply(xFrom));
            }
        }
    }

    Node getNode(int index) {
        Node adding;
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException("index went beyond");
        }

        if (index > (count / 2)) {
            adding = head.prev;
            for (int i = count - 1; i > 0; i--) {
                if (i == index) {
                    return adding;
                } else {
                    adding = adding.prev;
                }
            }
        } else {
            adding = head;
            for (int i = 0; i < count; i++) {
                if (index == i) {
                    return adding;
                } else {
                    adding = adding.next;
                }
            }
        }
        return adding;
    }

    //public double getX(int index) throws IllegalArgumentException {
    public double getX(int index)  {
        if (index < 0 || index >= count) {
            throw new IndexOutOfBoundsException("index went beyond");
        }
        return getNode(index).x;
    }

   // public double getY(int index) throws IllegalArgumentException {
   public double getY(int index){
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException("index went beyond");
        }
        return getNode(index).y;
    }

   // public void setY(int index, double value) throws IllegalArgumentException {
   public void setY(int index, double value) {
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException("index went beyond");
        }
        getNode(index).y = value;
    }

    public int indexOfX(double x) {
        Node adding;
        adding = head;
        for (int i = 0; i < count; i++) {
            if (adding.x == x) {
                return i;
            }
            adding = adding.next;
        }
        return -1;
    }

    public int indexOfY(double y) {
        Node adding;
        adding = head;
        for (int i = 0; i < count; i++) {
            if (adding.y == y) {
                return i;
            }
            adding = adding.next;
        }
        return -1;
    }

   // protected int floorIndexOfX(double x) throws IllegalArgumentException {
   protected int floorIndexOfX(double x){
       Node adding;
        if (x < head.x) {
            throw new IllegalArgumentException("the argument x is less than the minimum x in the function");
        }
        adding = head;
        for (int i = 0; i < count; i++) {
            if (adding.x < x) {
                adding = adding.next;
            } else {
                return i - 1;
            }
        }
        return getCount();
    }


    private Node floorNodeOfX(double x) throws IllegalArgumentException {
        Node adding;
        if (x < head.x) {
            throw new IllegalArgumentException("the argument x is less than the minimum x in the function");
        }
        adding = head;
        for (int i = 0; i < count; i++) {
            if (adding.x < x) {
                adding = adding.next;
            } else {
                return adding.prev;
            }
        }
        return head.prev;
    }

    @Override
    protected double extrapolateLeft(double x) {
        return interpolate(x, head.x, head.next.x, head.y, head.next.y);
    }

    @Override
    protected double extrapolateRight(double x) {
        return interpolate(x, head.prev.prev.x, head.prev.x, head.prev.prev.y, head.prev.y);
    }

   @Override
    protected double interpolate(double x, int floorIndex) throws InterpolationException {
        Node left = getNode(floorIndex);
        Node right = left.next;
        if (x < left.x || right.x < x) {
            throw new InterpolationException();
        }
        return interpolate(x, left.x, right.x, left.y, right.y);
    }

    @Override
    public double apply(double x) {
        if (x < leftBound()) {
            return extrapolateLeft(x);
        } else if (x > rightBound()) {
            return extrapolateRight(x);
        } else try {
            return nodeOfX(x).y;
        } catch (UnsupportedOperationException e) {
            Node left = floorNodeOfX(x);
            Node right = left.next;
            return super.interpolate(x, left.x, right.x, left.y, right.y);
        }
    }

    private Node nodeOfX(double x) {
        Node adding;
        adding = head;
        for (int i = 0; i < count; i++) {
            if (adding.x == x) {
                return adding;
            } else {
                adding = adding.next;
            }
        }
        throw new UnsupportedOperationException();
    }

    @Override
    public Iterator<Point> iterator() {
        return new Iterator<>() {
            private Node node = head;

            public boolean hasNext() {
                return node != null;
            }

            public Point next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }
                Point point = new Point(node.x, node.y);
                if (node != head.prev) {
                    node = node.next;
                } else {
                    node = null;
                }
                return point;
            }
        };
    }
}
