package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

public interface Removable {
    void remove(int index) throws IllegalArgumentException, ArrayIndexOutOfBoundsException;
}