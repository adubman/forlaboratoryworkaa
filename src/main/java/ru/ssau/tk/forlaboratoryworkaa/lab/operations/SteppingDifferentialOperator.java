package ru.ssau.tk.forlaboratoryworkaa.lab.operations;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.MathFunction;


abstract class SteppingDifferentialOperator implements DifferentialOperator<MathFunction> {

    protected double step;

    public SteppingDifferentialOperator(double step) {
       if ((step<=0)||(step!=step)||(step==(1.0 / 0.0))){throw new IllegalArgumentException("step is not positive or step equals positive infinity");}
                this.step = step;
    };

    public double getStep() {
        return step;
    };

    public void setStep(double step) {
        this.step = step;
    };

}
