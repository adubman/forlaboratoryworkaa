package ru.ssau.tk.forlaboratoryworkaa.lab.io;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.ArrayTabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.TabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.ArrayTabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.operations.TabulatedDifferentialOperator;

import java.io.*;

import static ru.ssau.tk.forlaboratoryworkaa.lab.io.FunctionsIO.deserialize;
import static ru.ssau.tk.forlaboratoryworkaa.lab.io.FunctionsIO.serialize;

public class ArrayTabulatedFunctionSerialization {
    public static void main(String[] args) {
        double[] xValues={3.4,4.5,5.5};
        double[] yValues={1.3,3.3,1.1};
        ArrayTabulatedFunction function = new ArrayTabulatedFunction(xValues, yValues);
        TabulatedDifferentialOperator differentialOperator = new TabulatedDifferentialOperator(new ArrayTabulatedFunctionFactory());
        TabulatedFunction firstDerive = differentialOperator.derive(function);
        TabulatedFunction secondDerive = differentialOperator.derive(firstDerive);
        try (BufferedOutputStream outPutStream = new BufferedOutputStream(new FileOutputStream("output/serialized array functions.bin"))) {
            serialize(outPutStream, function);
            serialize(outPutStream, firstDerive);
            serialize(outPutStream, secondDerive);
        } catch (IOException e) {
            e.printStackTrace();
        };
        try (BufferedInputStream inPutStream = new BufferedInputStream(new FileInputStream("output/serialized array functions.bin"))) {
            System.out.println(deserialize(inPutStream).toString() + '\n' + deserialize(inPutStream).toString() + '\n' + deserialize(inPutStream).toString());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        };
    };
}

