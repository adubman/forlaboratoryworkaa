package ru.ssau.tk.forlaboratoryworkaa.lab.ui;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.TabulatedFunction;
import javax.swing.table.AbstractTableModel;

public class TableModelUsingTabulatedFunction extends AbstractTableModel {
    private static final int X_NUMBER = 0;
    private static final int Y_NUMBER = 1;
    private TabulatedFunction tabulatedFunction;

    public TabulatedFunction getTabulatedFunction() {
        return tabulatedFunction;
    }

    public void setTabulatedFunction(TabulatedFunction tabulatedFunction) {
        this.tabulatedFunction = tabulatedFunction;
    }

    @Override
    public void setValueAt(Object bValue, int rowIndex, int columnIndex) throws NumberFormatException {
        if (tabulatedFunction == null) {
            return;
        }
        if (columnIndex == Y_NUMBER) {
            try {
                tabulatedFunction.setY(rowIndex, (Double) bValue);
            } catch (Exception e) {
                tabulatedFunction.setY(rowIndex, 0.0);
            }
        }
    }

    @Override
    public int getRowCount() {
        if (tabulatedFunction == null) {
            return 0;
        } else {
            return tabulatedFunction.getCount();
        }

    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case X_NUMBER:
                return "Value";
            case Y_NUMBER:
                return "Result";
        }
        return super.getColumnName(column);
    }

    @Override
    public Class getColumnClass(int columnIndex) {
        return Double.class;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return tabulatedFunction.getX(rowIndex);
            case 1:
                return tabulatedFunction.getX(columnIndex);
        }
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case X_NUMBER:
                return false;
            case Y_NUMBER:
                return true;
        }
        return false;
    }
}