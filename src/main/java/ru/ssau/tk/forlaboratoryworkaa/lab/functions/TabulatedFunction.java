package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

//import ru.ssau.tk.forlaboratoryworkaa.lab.ui.PointRecord;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.function.Function;

public interface TabulatedFunction extends MathFunction, Iterable<Point> {
   // int getPointCount();
   /* static TabulatedFunction readTabulatedFunction(FileReader reader) {
        return null;
    }

    static TabulatedFunction tabulate(Function function, double leftX, double rightX, int pointsCount) {
        return null;
    }

    static void writeTabulatedFunction(TabulatedFunction function, FileWriter writer) {
    }

    int getCount();

    double getX(int index);

    double getY(int index);

    void setY(int index, double value);

    int indexOfX(double x);

    int indexOfY(double y);

    double leftBound();

    double rightBound();

    void setX(int index);
}*/
   int getCount();

    double getX(int index);

    double getY(int index);

    void setY(int index, double value);

    int indexOfX(double x);

    int indexOfY(double y);

    double leftBound();

    double rightBound();

}
