package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import static java.lang.Math.*;

public class SqrFunction implements MathFunction {

    public double apply(double x) {
        return pow(x, 2);
    }
}