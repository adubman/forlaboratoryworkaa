package ru.ssau.tk.forlaboratoryworkaa.lab.exceptions;

public class InconsistentFunctionsException extends RuntimeException {
    private static final long serialVersionUID = -5957389065525593035L;

    public InconsistentFunctionsException() {
    }

    public InconsistentFunctionsException(String message) {
        super(message);
    }
}