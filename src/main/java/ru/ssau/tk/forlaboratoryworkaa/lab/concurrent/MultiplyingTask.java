package ru.ssau.tk.forlaboratoryworkaa.lab.concurrent;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.TabulatedFunction;

public class MultiplyingTask implements Runnable{
    private final TabulatedFunction tabulatedFunction;

    MultiplyingTask(TabulatedFunction tabulatedFunction) {
        this.tabulatedFunction = tabulatedFunction;
    }

    @Override
    public void run() {
        for (int i = 0; i < tabulatedFunction.getCount(); i++) {
                tabulatedFunction.setY(i,2*tabulatedFunction.getY(i));
        }
        System.out.println("Thread " + Thread.currentThread().getName() + " finished the task\n");
    }
}
