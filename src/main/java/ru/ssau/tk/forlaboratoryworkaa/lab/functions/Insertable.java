package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

public interface Insertable {
    void insert(double x, double y);
}