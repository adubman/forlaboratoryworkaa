package ru.ssau.tk.forlaboratoryworkaa.lab.ui;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.*;
import javax.swing.*;
import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import static ru.ssau.tk.forlaboratoryworkaa.lab.io.FunctionsIO.deserialize;
import static ru.ssau.tk.forlaboratoryworkaa.lab.io.FunctionsIO.serialize;

class MathWindow extends JFrame implements Serializable {
    private JComboBox<String> functionComboBox = new JComboBox<>();
    private JLabel fromLabel = new JLabel("from");
    private JLabel toLabel = new JLabel("to");
    private JLabel countLabel = new JLabel("count");
    private JTextField countField = new JTextField();
    private JTextField fromField = new JTextField();
    private JTextField toField = new JTextField();
    private JButton okButton = new JButton("OK");
    private TableModelUsingTabulatedFunction tableForFirstWindowModel;

    private JButton createButton= new JButton("Create");

    private Map<String, MathFunction> nameFunctionMap = new HashMap<>();

    MathWindow(TableModelUsingTabulatedFunction model) {
        super("Window");
        this.setBounds(250, 250, 250, 250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fillMap();
        compose();
        addButtonListeners();
        addButtonListenersTwo();
        okButton.setEnabled(true);
        this.tableForFirstWindowModel = model;
    }

    private void fillMap() {
        nameFunctionMap.put("atan", new AtanFunction());
        nameFunctionMap.put("sin", new SinFunction());
        nameFunctionMap.put("cos", new CosFunction());
        nameFunctionMap.put("log", new LogFunction());
        nameFunctionMap.put("exp", new ExpFunction());
        nameFunctionMap.put("sqr", new SqrFunction());
        nameFunctionMap.put("zero", new ZeroFunction());
        nameFunctionMap.put("unit", new UnitFunction());
        String[] MathWindow = new String[8];
        int i = 0;
        for (String string : nameFunctionMap.keySet()) {
            MathWindow[i++] = string;
        }
        Arrays.sort(MathWindow);
        for (String string : MathWindow) {
            functionComboBox.addItem(string);
        }
    }

    private void compose() {
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(fromLabel)
                        .addComponent(fromField)
                        .addComponent(toLabel)
                        .addComponent(toField)
                        .addComponent(countLabel)
                        .addComponent(countField))
                .addComponent(functionComboBox)
                .addComponent(okButton)
                .addComponent(createButton)
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(fromLabel)
                        .addComponent(fromField)
                        .addComponent(toLabel)
                        .addComponent(toField)
                        .addComponent(countLabel)
                        .addComponent(countField))
                .addComponent(functionComboBox)
                .addComponent(okButton)
                .addComponent(createButton)
        );
    }

    private void addButtonListeners() {
        okButton.addActionListener(event -> {
            String func = (String) functionComboBox.getSelectedItem();
            MathFunction selectedFunction = nameFunctionMap.get(func);
            double from = Double.parseDouble(fromField.getText());
            double to = Double.parseDouble(toField.getText());
            int count = Integer.parseInt(countField.getText());
            TabulatedFunction result = new LinkedListTabulatedFunction(selectedFunction, from, to, count);
            try (BufferedOutputStream outPutStream = new BufferedOutputStream(new FileOutputStream("Spring.txt"))) {
                serialize(outPutStream, result);
            } catch (IOException e) {
                ErrorWindows errorWindow = new ErrorWindows(this, e);
                errorWindow.showErrorWindow(this, e);
            }
            try (BufferedInputStream inPutStream = new BufferedInputStream(new FileInputStream("Spring.txt"))) {
                System.out.println(deserialize(inPutStream).toString());
            } catch (IOException | ClassNotFoundException e) {
                ErrorWindows errorWindow = new ErrorWindows(this, e);
                errorWindow.showErrorWindow(this, e);
            }
        });
    }

    private void addButtonListenersTwo() {
        createButton.addActionListener(event -> {
            String func = (String) functionComboBox.getSelectedItem();
            MathFunction selectedFunction = nameFunctionMap.get(func);
            double from = Double.parseDouble(fromField.getText());
            double to = Double.parseDouble(toField.getText());
            int count = Integer.parseInt(countField.getText());
            TabulatedFunction result = new LinkedListTabulatedFunction(selectedFunction, from, to, count);
            tableForFirstWindowModel.setTabulatedFunction(result);
            tableForFirstWindowModel.fireTableDataChanged();
        });
    }
}


