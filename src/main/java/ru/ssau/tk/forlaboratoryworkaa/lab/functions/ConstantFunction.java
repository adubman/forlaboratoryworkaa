package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

public class ConstantFunction implements MathFunction {
   private final double y;

    public double apply(double x) {
        return y;
    }

    public ConstantFunction(double x) {
        this.y =  x;
    }

   }