package ru.ssau.tk.forlaboratoryworkaa.lab.operations;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.MathFunction;

public interface DifferentialOperator<T extends MathFunction> {
    T derive(T function);
}
