package ru.ssau.tk.forlaboratoryworkaa.lab.operations;


import ru.ssau.tk.forlaboratoryworkaa.lab.functions.MathFunction;

public class LeftSteppingDifferentialOperator extends SteppingDifferentialOperator {

    protected double step;

    public LeftSteppingDifferentialOperator(double step) {
        super(step);
    };

    @Override
    public MathFunction derive(MathFunction function){
        MathFunction derivative = new MathFunction() {
            @Override
            public double apply(double x) {
                return (function.apply(x)-function.apply(x-step))/step;
            };
        };
        return derivative;
    };
}
