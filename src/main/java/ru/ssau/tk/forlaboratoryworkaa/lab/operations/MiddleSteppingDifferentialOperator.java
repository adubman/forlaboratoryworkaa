package ru.ssau.tk.forlaboratoryworkaa.lab.operations;


import ru.ssau.tk.forlaboratoryworkaa.lab.functions.MathFunction;

public class MiddleSteppingDifferentialOperator extends SteppingDifferentialOperator {

    protected double step;

    public MiddleSteppingDifferentialOperator(double step) {
        super(step);
    };

    @Override
    public MathFunction derive(MathFunction function){
        MathFunction derivative = new MathFunction() {
            @Override
            public double apply(double x) {
                return (function.apply(x+step)-function.apply(x-step))/2/step;
            };
        };
        return derivative;
    };
}
