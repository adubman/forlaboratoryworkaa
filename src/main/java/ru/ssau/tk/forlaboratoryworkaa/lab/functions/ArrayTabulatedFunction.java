package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.ArrayIsNotSortedException;
import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.DifferentLengthOfArraysException;
import ru.ssau.tk.forlaboratoryworkaa.lab.exceptions.InterpolationException;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayTabulatedFunction extends AbstractTabulatedFunction implements  Insertable, Removable{
    private double[] xValues;
    private double[] yValues;
    private int count;

    public ArrayTabulatedFunction(double[] xValues, double[] yValues) {
        checkLengthIsTheSame(xValues,yValues);
        checkSorted(xValues);
        if (xValues.length < 2) {
            throw new IllegalArgumentException("the number of points is less than two");
        }
        count = xValues.length;
        this.xValues = Arrays.copyOf(xValues, count);
        this.yValues = Arrays.copyOf(yValues, count);
    }

    public ArrayTabulatedFunction(MathFunction source, double xFrom, double xTo, int count) {
        if (count < 2) {
            throw new IllegalArgumentException("the number of points is less than two");
        }
        this.count = count;
        if (xFrom > xTo) {
            double xOdds;
            xOdds = xFrom - xTo;
            xTo = xTo + xOdds;
            xFrom = xFrom - xOdds;
        }

        xValues = new double[count];
        yValues = new double[count];
        double step = (xTo - xFrom) / (count - 1);
        double adding = xFrom;
        if (xFrom == xTo) {
            double functionXFrom = source.apply(xFrom);
            for (int i = 0; i < count; i++) {
                xValues[i] = xFrom;
                yValues[i] = functionXFrom;
                adding = adding + step;
            }
        } else {
            for (int i = 0; i < count; i++) {
                xValues[i] = adding;
                yValues[i] = source.apply(adding);
                adding = adding + step;
            }

        }
    }

    public ArrayTabulatedFunction() {
        xValues = new double[]{};
        yValues = new double[]{};
        count = 0;
    }

    public static TabulatedFunction getIdentity() {
        return new ArrayTabulatedFunction();
    }

    public int getCount() {
        return count;
    }

   // public double getX(int index) throws IllegalArgumentException {
   public double getX(int index){
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException("index went beyond");
        }
        return xValues[index];
    }

   // public double getY(int index) throws IllegalArgumentException {
   public double getY(int index){
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException("index went beyond");
        }
        return yValues[index];
    }

    //public void setY(int index, double value) throws IllegalArgumentException {
    public void setY(int index, double value){
        if (index < 0 || index >= count) {
            throw new IllegalArgumentException("index went beyond");
        }
        yValues[index] = value;
    }

    public double leftBound() {
        return xValues[0];
    }

    public double rightBound() {
        return xValues[count - 1];
    }

    public int indexOfX(double x) {
        for (int i = 0; i < count; i++) {
            if (xValues[i] == x) {
                return i;
            }
        }
        return -1;
    }

    public int indexOfY(double y) {
        for (int i = 0; i < count; i++) {
            if (yValues[i] == y) {
                return i;
            }
        }
        return -1;
    }

    //protected int floorIndexOfX(double x) throws IllegalArgumentException {
    protected int floorIndexOfX(double x){
        if (x < leftBound()) {
            throw new IllegalArgumentException("the argument x is less than the minimum x in the function");
        }
        for (int i = 0; i < count; i++) {
            if (xValues[i] > x) {
                return i - 1;
            }
        }
        return count;
    }

    protected double extrapolateLeft(double x) {
        return interpolate(x, xValues[0], xValues[1], yValues[0], yValues[1]);
    }

    protected double extrapolateRight(double x) {
        return interpolate(x, xValues[count - 2], xValues[count - 1], yValues[count - 2], yValues[count - 1]);
    }

    protected double interpolate(double x, int floorIndex) {
        if (x < xValues[floorIndex] || xValues[floorIndex + 1] < x) {
            throw new InterpolationException();
        }
        return interpolate(x, xValues[floorIndex], xValues[floorIndex + 1], yValues[floorIndex], yValues[floorIndex + 1]);
    }

    public Iterator<Point> iterator() {
        return new Iterator<>() {
            int i;

            public boolean hasNext() {
                return i != count;
            }

            public Point next() {
                if (i == count) {
                    throw new NoSuchElementException();
                }
                return new Point(xValues[i], yValues[i++]);
            }
        };
    }

    
    ////////////////
    @Override
    public void insert(double x, double y) {
        if (this.indexOfX(x) != -1) {
            setY(indexOfX(x), y);
        } else {
            double[] xValuesNew = new double[count + 1];
            double[] yValuesNew = new double[count + 1];
            if (floorIndexOfX(x) == 0) {
                System.arraycopy(xValues, 0, xValuesNew, 1, count);
                System.arraycopy(yValues, 0, yValuesNew, 1, count);
                xValuesNew[0] = x;
                yValuesNew[0] = y;
            }
            if (floorIndexOfX(x) == count) {
                System.arraycopy(xValues, 0, xValuesNew, 0, count);
                System.arraycopy(yValues, 0, yValuesNew, 0, count);
                xValuesNew[count + 1] = x;
                yValuesNew[count + 1] = y;
            } else {
                int i = floorIndexOfX(x);
                System.arraycopy(xValues, 0, xValuesNew, 0, i + 1);
                System.arraycopy(xValues, i + 1, xValuesNew, i + 2, count - i - 1);
                xValuesNew[i + 1] = x;
                System.arraycopy(yValues, 0, yValuesNew, 0, i + 1);
                System.arraycopy(yValues, i + 1, yValuesNew, i + 2, count - i - 1);
                yValuesNew[i + 1] = y;
            }
            count++;
            this.xValues = xValuesNew;
            this.yValues = yValuesNew;
        }
    }

    @Override
    public void remove(int index) throws ArrayIndexOutOfBoundsException {
        if (index < 0 || index >= count) {
            throw new ArrayIndexOutOfBoundsException("Index is out of bounds");
        }
        double[] xRemove = new double[count - 1];
        double[] yRemove = new double[count - 1];
        if (index == 0) {
            System.arraycopy(xValues, 1, xRemove, 0, count - 1);
            System.arraycopy(yValues, 1, yRemove, 0, count - 1);
        } else if (index == (count - 1)) {
            System.arraycopy(xValues, 0, xRemove, 0, count - 1);
            System.arraycopy(yValues, 0, yRemove, 0, count - 1);
        } else {
            System.arraycopy(xValues, 0, xRemove, 0, index);
            System.arraycopy(yValues, 0, yRemove, 0, index);
            System.arraycopy(xValues, index + 1, xRemove, index, (count - index - 1));
            System.arraycopy(yValues, index + 1, yRemove, index, (count - index - 1));
        }
        count--;
        this.xValues = xRemove;
        this.yValues = yRemove;
    }
}


