package ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.MathFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.StrictTabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.TabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.UnmodifiableTabulatedFunction;

public interface TabulatedFunctionFactory {
    TabulatedFunction create(double[] xValues, double[] yValues);

    default TabulatedFunction createStrict(double[] xValues, double[] yValues) {
        return new StrictTabulatedFunction(create(xValues, yValues));
    }

    default TabulatedFunction createStrictUnmodifiable(double[] xValues, double[] yValues) {
        return new UnmodifiableTabulatedFunction(new StrictTabulatedFunction(create(xValues, yValues)));
    }
    TabulatedFunction create(MathFunction source, double xFrom, double xTo, int count);

    TabulatedFunction getIdentity();
}
