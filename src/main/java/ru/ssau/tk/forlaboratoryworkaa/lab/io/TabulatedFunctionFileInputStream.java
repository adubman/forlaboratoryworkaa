package ru.ssau.tk.forlaboratoryworkaa.lab.io;

import ru.ssau.tk.forlaboratoryworkaa.lab.functions.ArrayTabulatedFunction;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.ArrayTabulatedFunctionFactory;
import ru.ssau.tk.forlaboratoryworkaa.lab.functions.factory.LinkedListTabulatedFunctionFactory;

import java.io.*;

public class TabulatedFunctionFileInputStream {
    public static void main(String[] args) {

        try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream("input/binary function.bin"))) {
            ArrayTabulatedFunction arrayTabulatedFunction = (ArrayTabulatedFunction) FunctionsIO.readTabulatedFunction(inputStream, new ArrayTabulatedFunctionFactory());
            System.out.println(arrayTabulatedFunction.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            System.out.println("������� ������ � �������� �������");
            BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
            System.out.println(FunctionsIO.readTabulatedFunction(read, new LinkedListTabulatedFunctionFactory()).toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
