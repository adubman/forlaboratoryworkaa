package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

public class SinFunction implements MathFunction {
    public double apply(double x) {
        return Math.sin(x);
    }

}