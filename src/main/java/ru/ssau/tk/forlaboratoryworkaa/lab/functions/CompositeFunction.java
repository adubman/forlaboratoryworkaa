package ru.ssau.tk.forlaboratoryworkaa.lab.functions;

public class CompositeFunction implements MathFunction {

    private MathFunction firstFunction;
    private MathFunction secondFunction;

    CompositeFunction(MathFunction firstFunction, MathFunction secondFunction) {
        this.secondFunction = secondFunction;
        this.firstFunction = firstFunction;
    }

    public double apply(double x) {
        return secondFunction.apply(firstFunction.apply(x));
    }
}
